# Description

PPD for printing to Xerox WorkCentre 7120 machines with accounting enabled. This PPD is based in 
[Generic-PCL_6_PCL_XL_Printer](https://www.openprinting.org/ppd-o-matic.php?driver=pxlcolor&printer=Generic-PCL_6_PCL_XL_Printer&show=1) 
and is therefore licensed under the GPLv2 license (see [COPYING](COPYING) for details).

The driver has been tested to work in Ubuntu 16.04 and Fedora 24 but should work in any OS running 
[CUPS](https://www.cups.org/) and [foomatic](https://wiki.linuxfoundation.org/openprinting/database/foomatic).

[TOC]

# Configuration
Open the PPD file and change "XXXXYOURPASSWORDHEREXXXX" for your accounting id. Then install the printer with the modified PPD.

If your distro/OS does not provide a GUI for installing a printer with a custom PPD, you can try using CUPS' 
[integrated web interface](http://localhost:631/admin/).

# Supported features
Colour and double-sided printing should work just fine.

## Do I need the PostScript module installed in the printer for this driver to work?

No; this driver outputs PCL XL commands so the (expensive) PS module is not required.

## How do I get the driver to work on macOS?

(Instructions adapted from those available [here](http://flying-geek.blogspot.com.es/2016/05/getting-os-x-1011-el-capitan-printing.html)).

I have tested the driver to work on macOS Sierra 10.12.5.

For macOS, use printer-macos.ppd instead of printer.ppd as your printer PPD file.

  * Install foomatic-rip 4.0.6 from the [Linux Foundation page](https://wiki.linuxfoundation.org/openprinting/macosx/foomatic).
  * Install Ghostscript from [Richard Koch's webpage](http://pages.uoregon.edu/koch/) (I used version 9.21)
  * Relax the CUPS sandbox by opening a terminal and running:

```bash
sudo echo "Sandboxing Relaxed" >> /etc/cups/cups-files.conf
sudo launchctl  stop org.cups.cupsd
sudo launchctl start org.cups.cupsd
```

  * Enable the CUPS web interface by executing the following command in Terminal.app:

```bash
cupsctl WebInterface=yes
```

  * Install the printer through [CUPS' integrated web interface](http://localhost:631/admin/). I had to add a LPD/LPR connection on socket://10.0.0.11 (10.0.0.11 is the printer's IP address), if that helps.
  * Print a test page or two
  * Disable the CUPS web interface by executing the following command in Terminal.app:

```bash
cupsctl WebInterface=no
```

You'll probably have to repeat the procedura after a major OS upgrade.

# Improvements

The driver works fine enough for my daily requirements but feel free to create pull requests with new features if you implement them.

Drop me a line to joseba.gar@gmail.com if you find the code useful!
